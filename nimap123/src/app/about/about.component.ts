import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ImgService } from '../img.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  tt: any;
  formData: any;
  imgURL: string | ArrayBuffer;
  SelectedFiles: File;
  isValid: boolean;

  constructor(public aa:FormBuilder, public img:ImgService) { }
  formDat:FormGroup;            
  emp:any
  
    ngOnInit() {
      this.img.eee().subscribe(data=>this.emp=data)

      this.formDat=this.aa.group({
        id:["",Validators.required],
        name:["",[Validators.maxLength(20),Validators.pattern("[a-z/A-Z]*")]],
        lastname:["",[Validators.maxLength(20),Validators.pattern("[a-z/A-Z]*")]],
        email:["",[Validators.required,Validators.email]],
        mobile:["",[Validators.required,Validators.pattern("[0-9]*"),
        Validators.minLength(10),Validators.maxLength(10)]],
        age:["",Validators.required],
        state:["",Validators.required],
        address:["",Validators.required],
        country:["",Validators.required],
        //image:["",[Validators.required,Validators.pattern("size=310*325")]]
      

    
      })
     
    }
    click(formDat){
      if(this.formData.valid){
        console.log("form submit",this.formData.value);
        this.img.sss(this.formData.value).subscribe()
        
      }
    }
    upload(event){

      if(event.target.files[0].size<310*325)
      {

      this.isValid=false;
      this.SelectedFiles=<File>event.target.files[0];
    
      var amir =new FileReader();
      amir.readAsDataURL(this.SelectedFiles);
      amir.onload = (_event) => {
       
        
        this.imgURL =amir.result;
        this.formDat.value["Profileimg"]=this.imgURL;
        }
      }
        else{
          this.isValid=true;
        }
      
   }
   update(){
     this.img.aaa(this.formDat.value).subscribe(
       res=>{
         return alert("upadatesuccefullyyyy")
       }

     )
   }
}