import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ImageComponent } from './image/image.component';


const routes: Routes = [
  {
    path:'',
    component:ImageComponent,
    pathMatch:"full"
  },

  {
    path:"image",
    component:ImageComponent,
  
  },
  
  {
    path:"about",
    component:AboutComponent,
  
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
