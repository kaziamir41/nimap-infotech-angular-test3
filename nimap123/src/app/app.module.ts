import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import  {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AboutComponent } from './about/about.component';

import { HttpClientModule } from '@angular/common/http';
import { FileUploadModule } from 'ng2-file-upload';
import { ImageComponent } from './image/image.component';
import { HeaderComponent } from './header/header.component';
import { ImgComponent } from './img/img.component';





@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
   ImgComponent,
    ImageComponent,
    HeaderComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FileUploadModule
   
    


  ],
  providers: [],
  bootstrap: [AppComponent],
  
  
  
})
export class AppModule { }
