import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImgService } from '../img.service';
import {MatChipInputEvent} from '@angular/material/chips';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  cityName: any;
  SelectedFiles: File = null;
  imgURL: string | ArrayBuffer;
  imagePath= File;
  abc: object;
  uploadedFilePath: string = null;
  http: any;

  constructor(private tt:FormBuilder, public img:ImgService) { }

  formData:FormGroup;

    ngOnInit() {

      this.formData=this.tt.group({
        id:["",Validators.required],
        name:["",[Validators.maxLength(20),Validators.pattern("[a-z/A-Z]*")]],
        lastname:["",[Validators.maxLength(20),Validators.pattern("[a-z/A-Z]*")]],
        email:["",[Validators.required,Validators.email]],
        mobile:["",[Validators.required,Validators.pattern("[0-9]*"),
        Validators.minLength(10),Validators.maxLength(10)]],
        age:["",Validators.required],
        state:["",Validators.required],
        address:["",Validators.required],
        country:["",Validators.required],
        //image:["",[Validators.required,Validators.pattern("size=310*325")]]
       
      });
}
click(formData){
  if(this.formData.valid){
    console.log("form submit",this.formData.value);
    this.img.sss(this.formData.value).subscribe()
  }
  return alert("submitedd")
}
upload(event){
  this.SelectedFiles=<File>event.target.files[0];

  var amir =new FileReader();
  amir.readAsDataURL(this.SelectedFiles);
  amir.onload = (_event) => {
    if(event.target.files[0].size<310*325)
    {
    this.imgURL =amir.result;
    this.formData.value["Profileimg"]=this.imgURL;
    }
    else{
      return alert ("image size is greter than 310*325 ");
    }
  }

  

}
}