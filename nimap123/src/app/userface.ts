export interface userface{
    name:string;
    lastname:string;
    email:any;
    mobile:number;
    age:number;
    state:string;
    country:string;
    address:string;
    Profileimg:string;
}